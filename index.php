<?php

switch ($_GET['url']) {
    case "":
    case "dashboard":
        $root = $_SERVER['DOCUMENT_ROOT'] . "/hoc-web-coban/bai4-MVC/ban-hang-2/";
        // require $root."/config.php";
        require_once $root . "/App/Controllers/DashboardController.php";
        $new = new DashboardController();
        $new->index();
        break;
    case "category":
    
    case "category/":

    case "category/index":
        $root = $_SERVER['DOCUMENT_ROOT'] . "/hoc-web-coban/bai4-MVC/ban-hang-2/";
        // require $root."/config.php";
        require_once $root . "/App/Controllers/CategoryController.php";
        $new = new CategoryController();
        $new->index();
        break;
    case "category/create":
        $root = $_SERVER['DOCUMENT_ROOT'] . "/hoc-web-coban/bai4-MVC/ban-hang-2/";
        // require $root."/config.php";
        require_once $root . "/App/Controllers/CategoryController.php";
        $new = new CategoryController();
        $new->create();
        break;

    case "category/edit":
        $root = $_SERVER['DOCUMENT_ROOT'] . "/hoc-web-coban/bai4-MVC/ban-hang-2/";
        // require $root."/config.php";
        require_once $root . "/App/Controllers/CategoryController.php";
        $id = $_GET['id'];
        $new = new CategoryController();
        $new->edit($id);
        break;

    case "category/create":
        $root = $_SERVER['DOCUMENT_ROOT'] . "/hoc-web-coban/bai4-MVC/ban-hang-2/";
        // require $root."/config.php";
        require_once $root . "/App/Controllers/CategoryController.php";
        $new = new CategoryController();
        $new->create();
        break;
    case "category/store":
        $root = $_SERVER['DOCUMENT_ROOT'] . "/hoc-web-coban/bai4-MVC/ban-hang-2/";
        // require $root."/config.php";
        require_once $root . "/App/Controllers/CategoryController.php";
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "POST") {
            $new = new CategoryController();
            $new->store($_REQUEST);
        } else {
            require_once "./404.php";
        }

        break;
    case "category/update":
        $root = $_SERVER['DOCUMENT_ROOT'] . "/hoc-web-coban/bai4-MVC/ban-hang-2/";
        // require $root."/config.php";
        require_once $root . "/App/Controllers/CategoryController.php";
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "POST") {
            $new = new CategoryController();
            $new->update($_REQUEST, $_GET['id']);
        } else {
            require_once "./404.php";
        }

        break;
    case "category/delete":
        $root = $_SERVER['DOCUMENT_ROOT'] . "/hoc-web-coban/bai4-MVC/ban-hang-2/";
        // require $root."/config.php";
        require_once $root . "/App/Controllers/CategoryController.php";
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "POST") {
            $new = new CategoryController();
            $new->delete($_GET['id']);
        } else {
            require_once "./404.php";
        }

        break;
    default:
        require_once "./404.php";
        break;
}
