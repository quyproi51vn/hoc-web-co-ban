<?php

$root = $_SERVER['DOCUMENT_ROOT'] . "hoc-web-coban/bai4-MVC/ban-hang-2";
// require $root."/config.php";
require $root . "/App/Models/CategoryModel.php";

class CategoryController
{

    public function index()
    {
        try {
            //code...
            $list_category = CategoryModel::getAll();
            $root = $_SERVER['DOCUMENT_ROOT'] . "hoc-web-coban/bai4-MVC/ban-hang-2";
            require_once $root . "/App/Views/CMS/Pages/Category/index.php";
        } catch (\Throwable $th) {
            //throw $th;
            echo $th->getMessage();
        }
    }

    public function create()
    {
        try {
            //code...
            $list_category = CategoryModel::getAll();
            $root = $_SERVER['DOCUMENT_ROOT'] . "hoc-web-coban/bai4-MVC/ban-hang-2";
            require_once $root . "/App/Views/CMS/Pages/Category/create.php";
        } catch (\Throwable $th) {
            //throw $th;
            echo $th->getMessage();
        }
    }

    public function store($request)
    {
        try {
            //code...
            $new_category = new CategoryModel(null, $request['name'], $request['parent_id']);
            $res = CategoryModel::add($new_category);

            $response = [
                'status' => 'success',
                'message' => "Add successfully"
            ];
            if (!$res) {
                $response = [
                    'status' => 'danger',
                    'message' => "Add error"
                ];
            }
            $root = $_SERVER['DOCUMENT_ROOT'] . "hoc-web-coban/bai4-MVC/ban-hang-2";
            require_once $root . "/App/Views/CMS/Pages/Category/create.php";
        } catch (\Throwable $th) {
            //throw $th;
            echo $th->getMessage();
        }
    }

    public function edit($id)
    {
        try {
            //code...
            $root = $_SERVER['DOCUMENT_ROOT'] . "hoc-web-coban/bai4-MVC/ban-hang-2";
            $category = CategoryModel::find($id);
            $list_category = CategoryModel::getAll();
            if ($category == null) {
                require_once $root . "/404.php";
            }
            require_once $root . "/App/Views/CMS/Pages/Category/edit.php";
        } catch (\Throwable $th) {
            //throw $th;
            echo $th->getMessage();
        }
    }

    public function update($request, $id)
    {
        try {
            //code...
            $root = $_SERVER['DOCUMENT_ROOT'] . "hoc-web-coban/bai4-MVC/ban-hang-2";
            $update_category = new CategoryModel($id, $request['name'], $request['parent_id']);
            $res = CategoryModel::update($update_category);
            $list_category = CategoryModel::getAll();
            $response = [
                'status' => 'success',
                'message' => "Update successfully"
            ];
            $category = CategoryModel::find($id);
            if (!$res) {
                $response = [
                    'status' => 'danger',
                    'message' => "Update error"
                ];
            }
            require_once $root . "/App/Views/CMS/Pages/Category/edit.php";
        } catch (\Throwable $th) {
            //throw $th;
            echo $th->getMessage();
        }
    }

    public function delete($id)
    {
        try {
            //code...
            $res = CategoryModel::delete($id);
            $response = [
                'status' => 'success',
                'message' => "Update successfully"
            ];
            if (!$res) {
                $response = [
                    'status' => 'danger',
                    'message' => "Update error"
                ];
            }
            header("Location: http://localhost:81/hoc-web-coban/bai4-MVC/ban-hang-2/category");
        } catch (\Throwable $th) {
            //throw $th;
            echo $th->getMessage();
        }
    }
}
