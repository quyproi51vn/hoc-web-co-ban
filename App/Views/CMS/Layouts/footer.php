
  <!-- Bootstrap core JavaScript-->
  <script src="<?=$path?>/Asset/vendor/jquery/jquery.min.js"></script>
  <script src="<?=$path?>/Asset/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?=$path?>/Asset/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?=$path?>/Asset/js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <!-- <script src="<?=$path?>/Asset/vendor/chart.js/Chart.min.js"></script> -->

  <!-- Page level custom scripts -->
  <!-- <script src="<?=$path?>/Asset/js/demo/chart-area-demo.js"></script> -->
  <!-- <script src="<?=$path?>/Asset/js/demo/chart-pie-demo.js"></script> -->

</body>

</html>
