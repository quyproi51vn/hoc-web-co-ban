<?php
$root = $_SERVER['DOCUMENT_ROOT'] . "/hoc-web-coban/bai4-MVC/ban-hang-2/";
include_once $root . "/App/Views/CMS/Layouts/header.php";
include_once $root . "/App/Views/CMS/Layouts/sidebar.php";
include_once $root . "/App/Views/CMS/Layouts/top-bar.php";

?>

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Category</h1>
        <div>
            <a href="<?=$path?>category/create"><button class="btn btn-primary">ADD NEW</button></a>
        </div>
    </div>

    <!-- Content Row -->
    <div class="shadow">
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Parent</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($list_category as $key => $category){ ?>
                <tr>
                    <td><?=$category->id?></td>
                    <td><?=$category->name?></td>
                    <td><?=$category->parent_id?></td>
                    <td class="d-flex">
                        <button class="mr-3 btn btn-danger" onClick="confirm('Do you want delele it?')?$('#form-delte-<?=$category->id?>').submit():''">Delete</button>
                        <form action="<?=$path?>category/delete?id=<?=$category->id?>" method="POST" id="form-delte-<?=$category->id?>" method="POST">
                        </form>
                        <a href="<?=$path?>category/edit?id=<?=$category->id?>"><button class="btn btn-info" >Edit</button></a>
                    </td>
                </tr>
                <?php }?>

            </tbody>

        </table>
    </div>

</div>
<!-- /.container-fluid -->

<?php
include_once $root . "/App/Views/CMS/Layouts/footer-body.php";
include_once $root . "/App/Views/CMS/Layouts/footer.php";
?>