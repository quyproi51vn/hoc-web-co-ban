<?php
$root = $_SERVER['DOCUMENT_ROOT'] . "/hoc-web-coban/bai4-MVC/ban-hang-2/";
// require $root."/config.php";

require_once $root . "/App/Views/CMS/Layouts/header.php";
require_once $root . "/App/Views/CMS/Layouts/sidebar.php";
require_once $root . "/App/Views/CMS/Layouts/top-bar.php";

?>

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="d-flex">
            <a href="<?= $path . 'category' ?>">
                <button class="btn btn-primary">Back</button>
            </a>
            <h1 class="ml-3 h3 mb-0 text-gray-800">
                Edit Category
            </h1>

        </div>
        <div></div>
    </div>

    <!-- Content Row -->
    <div class="container">
        <?php if (isset($response)) { ?>
            <div class="alert alert-<?= $response['status'] ?>" role="alert">
                <?= $response['message'] ?>
            </div>
        <?php } ?>
        <form action="<?=$path?>category/update?id=<?=$category->id?>" method="POST">
            <div class="form-group">
                <label for="">
                    Name
                </label>
                <input type="text" value="<?= $category->name ?>" name="name" class="form-control">
            </div>

            <div class="form-group">
                <label for="">
                    Parent
                </label>
                <select name="parent_id" class="form-control" id="">
                    <option value="null">None</option>
                    <?php
                    foreach ($list_category as $key => $value) {
                        if ($value->id != $category->id) {
                            ?>
                            <option value="<?= $value->id ?>" <?php if ($value->id == $category->parent_id) echo "selected='true'" ?>>
                                <?= $value->name ?>
                            </option>
                    <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <button class="btn btn-primary" type="submit"> Submit</button>
        </form>
    </div>

</div>
<!-- /.container-fluid -->

<?php
require_once $root . "/App/Views/CMS/Layouts/footer-body.php";
require_once $root . "/App/Views/CMS/Layouts/footer.php";
?>