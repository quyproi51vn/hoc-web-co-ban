<?php

include_once "./DBConnect.php";

class CategoryModel extends DBConnect
{

    var $id;
    var $name;
    var $parent_id;

    public function __construct($id, $name, $parent_id)
    {
        $this->id = $id;
        $this->name = $name;
        $this->parent_id = $parent_id;
    }

    static public function add($category)
    {
        $db = static::connect();
        $sql = "INSERT INTO categories (name, parent_id) VALUES ('" . $category->name . "', " . $category->parent_id . ")";

        $result = true;
        if ($db->query($sql) === TRUE) {
            $result = true;
        } else {
            $result = false;
            throw new Exception("Error: " . $sql . "<br>" . $db->error);
        }
        return $result;
    }

    static public function update($category)
    {
        $db = static::connect();
        $sql = "UPDATE categories SET `name`='" . $category->name . "', `parent_id`=" . $category->parent_id . " WHERE `id`=" . $category->id;
        $result = true;
        if ($db->query($sql) === TRUE) {
            $result = true;
        } else {
            $result = false;
            throw new Exception("Error: " . $sql . "<br>" . $db->error);
        }
        return $result;
    }

    static public function delete($id)
    {
        $db = static::connect();
        $sql = "DELETE FROM categories WHERE `id`=" . $id;;
        $result = true;
        if ($db->query($sql) === TRUE) {
            $result = true;
        } else {
            $result = false;
            throw new Exception("Error: " . $sql . "<br>" . $db->error);
        }
        return $result;
    }

    static public function getAll()
    {
        $db = static::connect();
        $sql = "SELECT * FROM categories";

        $result = $db->query($sql);
        $ls = [];
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
               $ls[] = new CategoryModel($row['id'],$row['name'],$row['parent_id']);
            }
        } else {
            echo "0 results";
        }
        $db->close();

        return $ls;
    }

    static public function find($id)
    {
        $db = static::connect();
        $sql = "SELECT * FROM categories WHERE `id`=".$id;

        $result = $db->query($sql);
        $data = null;
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
               $data = new CategoryModel($row['id'],$row['name'],$row['parent_id']);
            }
        }
        $db->close();

        return $data;
    }
}
