<?php
class DBConnect
{
    private static $conn = null;
    protected static function connect()
    {
        $servername = "mysql";
        $username = "root";
        $password = "root";
        $databaseName = "webBanHangMVC";
        // Create connection
        $_conn = self::$conn;
        if($_conn != null)
            return $_conn;
        
        $_conn = new mysqli($servername, $username, $password,$databaseName);

        // Check connection
        if ($_conn->connect_error) {
            die("Connection failed: " . $_conn->connect_error);
        }
        return $_conn;
    }
}
